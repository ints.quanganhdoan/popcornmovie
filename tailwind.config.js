/** @type {import('tailwindcss').Config} */
export default {
  mode: 'jit',
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '4rem',
        xl: '5rem',
        '2xl': '6rem',
      },
    },
    extend: {
      colors: {
        primaryColor: '#8E182E'
      },
      backgroundImage: {
        backgroundError: "url('/Error/backgroundError.jpg')"
      },
      fontFamily: {
        primaryFont: ['Montserrat', 'sans-serif'],
        secondaryFont: ['Limelight', 'sans-serif'],
      }
    },
  },
  plugins: [],
}