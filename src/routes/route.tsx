import ErrorPage from "../Error/ErrorPage";
import PageDetailsMovie from "../components/Page/PageDetailsMovie";
import PageDetailsTV from "../components/Page/PageDetailsTV";
import PageDetailsTVSeason from "../components/Page/PageSeasonEpsiode";
import PageSearch from "../components/Page/PageSearch";
import ComingSoon from "../layouts/ComingSoon";
import Genres from "../layouts/Genres";
import Home from "../layouts/Home";
import MovieLayout from "../layouts/MovieLayout";
import Newest from "../layouts/Newest";
import Popular from "../layouts/Popular";
import Series from "../layouts/Series";
import TopRated from "../layouts/TopRated";

export const route = [
    {
        path: "/",
        element: <MovieLayout />,
        errorElement: <ErrorPage />,
        children: [
            {
                path:"/",
                element: <Home />
            },
            {
                path:"/coming_soon/:page",
                element: <ComingSoon />,
            },
            {
                path:"/home/newest/:page",
                element: <Newest />
            },
            {
                path:"/home/top_rated/:page",
                element: <TopRated />
            },
            {
                path:"/home/popular/:page",
                element: <Popular />
            },
            {
                path:"/series/:page",
                element: <Series />
            },
            {
                path:"/genres/:genres/:page",
                element: <Genres />
            },
            {
                path:"/search/:query/:page",
                element: <PageSearch />
            },
            {
                path: "/movie_details/:id",
                element: <PageDetailsMovie />
            },
            {
                path: "/tv_details/:id/:name",
                element: <PageDetailsTV />
            },
            {
                path: "/tv_details/:id/:name/season/:season_number",
                element: <PageDetailsTVSeason />
            }
        ]
    }
]


export const routeToItem : {[key: string]: string} = {
    "/coming_soon": "Coming Soon",
    "/series": "Series TV",
    "/genres": "Genres",
    "/search": "Search",
    "/movie_details": "Details",
    "/tv_details": "Details",
};