import { configureStore } from '@reduxjs/toolkit'

import popularReducer from './features/MovieList/popularSlice'
import topRatedReducer from './features/MovieList/topRatedSlice'
import newestReducer from './features/MovieList/newestSlice'
import recommendReducer from './features/MovieList/recommendSlice'

import logger from 'redux-logger'

import comingSoonPageReducer from './features/MoviePage/comingSoonPageSlice'
import topRatedPageReducer from './features/MoviePage/topRatedPageSlice'
import popularPageReducer from './features/MoviePage/popularPageSlice'
import newestPageReducer from './features/MoviePage/newestPageSlice'
import seriesPageReducer from './features/MoviePage/seriesPageSlice'

import genresReducer from './features/MovieGenres/genresSlice'
import genresPageReducer from './features/MovieGenres/genresPageSlice'

import searchPageReducer from './features/MovieSearch/searchPageSlice'

import movieDetailsPageReducer from './features/DetailsPage/MovieDetailsPageSlice'
import movieDetailsTrailerReducer from './features/DetailsPage/MovieDetailsTrailerSlice'
import movieDetailsReviewReducer from './features/DetailsPage/MovieDetailsReviewSlice'

import  tvDetailsPageReducer  from './features/DetailsPage/TVDetailsPageSlice';
import tvSeasonEpisodeReducer  from './features/DetailsPage/TVSeasonEpisodePageSlice'
import EpisodeTrailerReducer from './features/DetailsPage/TVSeasonEpisodeTrailerSlice'



export const store = configureStore({
  reducer: {
    popularList: popularReducer,
    topRatedList: topRatedReducer,
    newestList: newestReducer,
    recommendList: recommendReducer,
    genresList: genresReducer,

    comingSoonPage: comingSoonPageReducer,
    newestPage: newestPageReducer,
    topRatedPage: topRatedPageReducer,
    popularPage: popularPageReducer,
    seriesPage: seriesPageReducer,
    genresPage: genresPageReducer,
    searchPage: searchPageReducer,

    movieDetailsPage: movieDetailsPageReducer,
    movieDetailsTrailer: movieDetailsTrailerReducer,
    movieDetailsReview: movieDetailsReviewReducer,

    tvDetailsPage: tvDetailsPageReducer,
    tvSeasonEpisode: tvSeasonEpisodeReducer,
    tvEpisodeTrailer: EpisodeTrailerReducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger)
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch