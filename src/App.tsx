import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { route } from "./routes/route";
import { Provider } from "react-redux";
import { store } from "./store";


const App = () => {
  const router = createBrowserRouter(route);
  return(
    <>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
    </>
  )
}

export default App
