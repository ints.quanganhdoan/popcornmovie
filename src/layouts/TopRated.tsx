import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../features/hooks/hooks";
import PageMovie from "../components/Page/PageMovie";
import Pagination from "../components/common/Pagination";
import { fetchTopRatedPageData } from "../features/MoviePage/topRatedPageSlice";
import { Link, useNavigate, useParams } from "react-router-dom";
import { RootState } from "../store";
import { scrollToTop } from "../components/common/ScrollButton";

const TopRated = () => {

    const dispatch = useAppDispatch();
    const topRatedPage = useAppSelector((state:RootState) => state.topRatedPage);

    const {page} = useParams();
    const currentPage = page ? parseInt(page) : 1;

    const navigate = useNavigate();

    const setPage = (newPage:number) => {
        navigate(`/home/top_rated/${newPage}`);
    }

    useEffect(() => {
        dispatch(fetchTopRatedPageData(currentPage));
        scrollToTop();
    },[dispatch, currentPage])

    

    return (
        <section>
            <div className="container mt-3">
                <div className="text-center text-primaryColor font-semibold mb-3">
                    <Link to={"/"} className="text-blue-500">Home</Link> / Top Rated
                </div>
                <PageMovie list={topRatedPage}/>
                <Pagination list={topRatedPage} setPage={setPage}/>
            </div>
        </section>
    );
};

export default TopRated;