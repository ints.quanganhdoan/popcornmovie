import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../features/hooks/hooks";
import PageMovie from "../components/Page/PageMovie";
import Pagination from "../components/common/Pagination";
import { fetchPopularPageData } from "../features/MoviePage/popularPageSlice";
import { Link, useNavigate, useParams } from "react-router-dom";
import { RootState } from "../store";
import { scrollToTop } from "../components/common/ScrollButton";

const TopRated = () => {

    const dispatch = useAppDispatch();
    const popularPage = useAppSelector((state:RootState) => state.popularPage);

    const { page } = useParams();
    const currentPage = page ? parseInt(page) : 1;

    const navigate = useNavigate();

    const setPage = (newPage:number) => {
        navigate(`/home/popular/${newPage}`);
    }

    useEffect(() => {
        dispatch(fetchPopularPageData(currentPage));
        scrollToTop();
    },[dispatch, currentPage])

    return (
        <section>
            <div className="container mt-3">
                    <div className="text-center text-primaryColor font-semibold mb-3">
                        <Link to={"/"} className="text-blue-500">Home</Link> / Popular
                    </div>
                    <PageMovie list={popularPage}/>
                    <Pagination list={popularPage} setPage={setPage}/>
            </div>
        </section>
    );
};

export default TopRated;