import { useEffect} from "react";
import { useAppDispatch, useAppSelector } from "../features/hooks/hooks";
import { fetchComingSoonPageData } from "../features/MoviePage/comingSoonPageSlice";
import PageMovie from "../components/Page/PageMovie";
import Pagination from "../components/common/Pagination";
import { RootState } from "../store";
import { useNavigate, useParams } from "react-router-dom";
import { scrollToTop } from "../components/common/ScrollButton";

const ComingSoon = () => {
    const dispatch = useAppDispatch();
    const comingSoonPage = useAppSelector((state: RootState) => state.comingSoonPage);

    const { page } = useParams();
    const currentPage = page ? parseInt(page) : 1;

    const navigate = useNavigate();

    const setPage = (newPage:number) => {
        navigate(`/coming_soon/${newPage}`);
    }

    useEffect(() => {
        dispatch(fetchComingSoonPageData(currentPage));
        scrollToTop();
    }, [dispatch, currentPage]);

    return (
        <section>
            <div className="container mt-3">
                <h1 className="text-center text-primaryColor font-semibold mb-3">Upcoming Movie</h1>
                <PageMovie list={comingSoonPage} />
                <Pagination 
                list={comingSoonPage} 
                setPage={(newPage) => setPage(newPage)} />
            </div>
        </section>
    );
};

export default ComingSoon;
