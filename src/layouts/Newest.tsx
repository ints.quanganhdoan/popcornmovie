import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../features/hooks/hooks";
import PageMovie from "../components/Page/PageMovie";
import Pagination from "../components/common/Pagination";
import { fetchNewestPageData } from "../features/MoviePage/newestPageSlice";
import { Link, useNavigate, useParams } from "react-router-dom";
import { RootState } from "../store";
import { scrollToTop } from "../components/common/ScrollButton";

const Newest = () => {

    const dispatch = useAppDispatch();
    const newestPage = useAppSelector((state:RootState) => state.newestPage);

    const { page } = useParams();
    const currentPage = page ? parseInt(page) : 1;

    const navigate = useNavigate();

    const setPage = (newPage:number) => {
        navigate(`/home/newest/${newPage}`);
    }

    useEffect(() => {
        dispatch(fetchNewestPageData(currentPage));
        scrollToTop();
    },[dispatch, currentPage])

    return (
        <section>
            <div className="container mt-3">
                    <div className="text-center text-primaryColor font-semibold mb-3">
                        <Link to={"/"} className="text-blue-500">Home</Link> / Newest
                    </div>
                    <PageMovie list={newestPage}/>
                    <Pagination list={newestPage} setPage={setPage}/>
            </div>
        </section>
    );
};

export default Newest;