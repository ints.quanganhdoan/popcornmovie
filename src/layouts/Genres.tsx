import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../features/hooks/hooks";
import { fetchGenresListData } from "../features/MovieGenres/genresSlice";
import { fetchGenresPageData } from "../features/MovieGenres/genresPageSlice";
import PageMovie from "../components/Page/PageMovie";
import Pagination from "../components/common/Pagination";
import ListGenres from "../components/ListGenres";
import { RootState } from "../store";
import { useNavigate, useParams } from "react-router-dom";

const Genres = () => {
    const dispatch = useAppDispatch();
    const genresList = useAppSelector((state: RootState) => state.genresList);
    const genresPage = useAppSelector((state: RootState) => state.genresPage);

    const [active, setActive] = useState<string>('');
    const [genresId, setGenresId] = useState<number>(0);
    const {genres} = useParams();

    const { page } = useParams();
    const currentPage = page ? parseInt(page) : 1;

    const navigate = useNavigate();

    useEffect(() => {
        dispatch(fetchGenresListData());
    }, []);

    const setPage = (newPage: number) => {
        navigate(`/genres/${active}/${newPage}`);
    }

    useEffect(() => {
        if (genresList.genres && genresList.genres.length > 0) {
            setActive(genres as string);
            const selectedGenre = genresList.genres.find(genre => genre.name === genres);
            if (selectedGenre) {
                setGenresId(selectedGenre.id);
            }
        }
    }, [genresList, genres]);

    useEffect(() => {
        if (active && genresId) {
            dispatch(fetchGenresPageData({ page: currentPage, genresId: genresId }));
        }
    }, [active, genresId, currentPage]);
    
    const handleClick = (name: string, genresId: number) => {
        setActive(name);
        setGenresId(genresId);
        navigate(`/genres/${name}/1`);
    }

    return (
        <div className="container mt-3">
            <ListGenres active={active} list={genresList.genres} onClick={(name, id) => handleClick(name, id)} />
            <div className="flex flex-col justify-center">
                <PageMovie list={genresPage} />
                <Pagination list={genresPage} setPage={(newPage) => setPage(newPage)} />
            </div>
        </div>
    );
};

export default Genres;
