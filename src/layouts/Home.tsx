import NewestList from '../components/MovieList/NewestList';
import PopularList from '../components/MovieList/PopularList';
import RecommendList from '../components/MovieList/RecommendList';
import TopRatedList from '../components/MovieList/TopRatedList';

const Home = () => {
    return (
        <div className='container flex-col justify-between mt-3'>
          <section>
            <NewestList />
            <TopRatedList />
            <PopularList />
          </section>
          <section>
            <RecommendList />
          </section>
        </div>
    );
};

export default Home;