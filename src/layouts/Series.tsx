import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../features/hooks/hooks";
import PageTV from "../components/Page/PageTV";
import Pagination from "../components/common/Pagination";
import { fetchSeriesPageData } from "../features/MoviePage/seriesPageSlice";
import { RootState } from "../store";
import { useNavigate, useParams } from "react-router-dom";
import { scrollToTop } from "../components/common/ScrollButton";

const Series = () => {

    const dispatch = useAppDispatch();
    const seriesPage = useAppSelector((state:RootState) => state.seriesPage);

    const { page } = useParams();
    const currentPage = page ? parseInt(page) : 1;

    const navigate = useNavigate();

    const setPage = (newPage:number) => {
        navigate(`/series/${newPage}`);
    }

    useEffect(() => {
        dispatch(fetchSeriesPageData(currentPage));
        scrollToTop();
    },[dispatch, currentPage])

    return (
        <section>
            <div className="container mt-3">
                <h1 className="text-center text-primaryColor font-semibold mb-3">Series Movie</h1>
                <PageTV list={seriesPage}/>
                <Pagination list={seriesPage} setPage={setPage}/>
            </div>
        </section>
    );
};

export default Series;