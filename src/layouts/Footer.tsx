const Footer = () => {
    return (
        <div className='flex items-center justify-center bg-primaryColor text-white  text-sm h-7 mt-4 
        lg:text-base h-9'>
           2023 Mask Movie © All rights reserved 
        </div>
    );
};

export default Footer;