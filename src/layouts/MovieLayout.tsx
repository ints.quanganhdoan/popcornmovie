import Header from './Header';
import SearchInput from '../components/SearchInput';
import { Outlet } from 'react-router-dom';
import Footer from './Footer';
import ScrollButton from '../components/common/ScrollButton';

const MovieLayout = () => {
    return (
        <>
        <header>
          <Header />
          <SearchInput />
        </header>
        <main className='flex justify-center'>
            <Outlet />
            <ScrollButton />
        </main>
        <footer>
            <Footer />
        </footer>
        </>
    );
};

export default MovieLayout;