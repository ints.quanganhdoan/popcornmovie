import { useEffect, useState } from 'react';
import Button from '../components/common/Button';
import {GiPopcorn} from "react-icons/gi"
import { navMenuData } from '../data/navMenuData';
import NavMenu from '../components/NavMenu';
import useResizeHandling from '../hooks/useResizeHandling';
import { useLocation } from 'react-router-dom';

const Header = () => {

    const [isOpen, setIsOpen] = useState<boolean>(false);

    const handleResizeEvent = () => useResizeHandling(setIsOpen);

    useEffect(() => {
        handleResizeEvent();
        window.addEventListener('resize', handleResizeEvent);

        return () => {
          window.removeEventListener('resize', handleResizeEvent);
        };
    }, []);

    const location = useLocation();

    useEffect(() => {
      if (location.pathname.includes('/search')) {
        handleResizeEvent();
      }
    }, [location.pathname]);


    return (
        <div className='bg-primaryColor text-white'>
            <div className='flex flex-col
            lg:flex-row justify-center'>
                <div className='flex justify-between items-center px-3 py-2'>
                    <Button onClick={() => setIsOpen(!isOpen)} className='w-5 h-5 lg:hidden'>
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} 
                                d={isOpen ? "M6 18L18 6M6 6l12 12" : "M4 6h16M4 12h16m-7 6h7"} />
                        </svg>
                    </Button>
                    <h1 className='text-center text-sm font-bold font-secondaryFont
                    lg:text-base'>
                        Popcorn Movie
                    </h1>
                    <GiPopcorn className='text-2xl lg:hidden'/>
                </div>
            </div>
                <NavMenu data={navMenuData} isOpen={isOpen} setIsOpen={setIsOpen}/>
        </div>
    );
};

export default Header