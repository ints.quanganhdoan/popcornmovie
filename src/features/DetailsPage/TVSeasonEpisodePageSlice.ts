import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { useFetchPage } from "../hooks/useFetchPage";
import { baseUrlTV } from "../../environment/environment";

//define type

export interface SeasonPageState {
    name: string,
    air_date: string,
    overview: string,
    still_path: string,
    vote_average: number,
    status: 'idle' | 'loading' | 'succeeded' | 'failed',
    error: string | undefined,
}

export interface SeasonDataType {
    id: string|undefined,
    season_number: string | undefined,
    episode_number: string | undefined
}

//initialState
const initialState : SeasonPageState = {
    name: '',
    air_date: '',
    overview: '',
    still_path: '',
    vote_average: 0,
    status: 'idle',
    error: undefined
}

//call API
export const fetchTVSeasonEpisodeData = createAsyncThunk('detailsTVSeasonData/fetch', async({id, season_number, episode_number}: SeasonDataType) => {
    return useFetchPage(`${baseUrlTV}/${id}/season/${season_number}/episode/${episode_number}?language=en-US`);
})

//create Slice
export const tvDetailsSeasonEpisodeSlice = createSlice({
    name: 'TVSeasonData',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
        .addCase(fetchTVSeasonEpisodeData.pending, (state) => {
          state.status = "loading";
        })
        .addCase(fetchTVSeasonEpisodeData.fulfilled, (state, action) => {
          state.status = "succeeded";
          state.name = action.payload.name,
          state.air_date = action.payload.air_date,
          state.overview = action.payload.overview,
          state.still_path = action.payload.still_path,
          state.vote_average = action.payload.vote_average
        })
        .addCase(fetchTVSeasonEpisodeData.rejected,(state ,action) =>{
          state.status = "failed";
          state.error = action.error.message;
        }) 
    }
})

export default tvDetailsSeasonEpisodeSlice.reducer;