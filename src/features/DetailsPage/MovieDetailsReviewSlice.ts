import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { accessKey, baseUrlMovie } from '../../environment/environment';

//define type
interface AuthorDetailsType{
    avatar_path: string,
    rating: number,
}

export interface ReviewType{
    author: string,
    author_details: AuthorDetailsType,
    content: string
}

export interface ReviewPageState {
    page: number,
    reviews: ReviewType[],
    total_pages: number,
    status: 'idle' | 'loading' | 'succeeded' | 'failed';
    error: string | undefined;
}

//initialState
const initialState : ReviewPageState = {
    page: 1,
    reviews: [],
    total_pages: 0,
    status: 'idle',
    error: undefined
}

// call API
export const fetchReviewsData = createAsyncThunk('reiviewMovie/fetch', async({id, page}:{id:string|undefined, page: number}) =>{
    const response = await axios.get(`${baseUrlMovie}/${id}/reviews?language=en-US&page=${page}`, {
        headers: {
          Authorization: `Bearer ${accessKey}`,
          'Content-Type': 'application/json;charset=utf-8',
        },
      });
      
       return response.data;
})

// create Slice
export const reviewSlice = createSlice({
    name: 'review',
    initialState,
    reducers:{},
    extraReducers: (builder) => {
        builder
        .addCase(fetchReviewsData.pending, (state) => {
            state.status = "loading";
          })
          .addCase(fetchReviewsData.fulfilled, (state, action) => {
            state.status = "succeeded";
            state.page = action.payload.page;
            state.total_pages = action.payload.total_pages;
            state.reviews = action.payload.results; 
          })
          .addCase(fetchReviewsData.rejected,(state ,action) =>{
            state.status = "failed";
            state.error = action.error.message;
          })  
    }
})

export default reviewSlice.reducer;