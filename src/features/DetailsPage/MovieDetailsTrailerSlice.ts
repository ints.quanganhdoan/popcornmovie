import { accessKey, baseUrlMovie } from "../../environment/environment";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axios from "axios";

//define type
export interface VideoType{
    id: string,
    key: string,
    name: string,
    type: string
}

interface VideoListType {
    videos: VideoType[],
    status: 'idle' | 'loading' | 'succeeded' | 'failed',
    error: string|undefined
}

//create initialState
const initialState : VideoListType = {
    videos: [],
    status: 'idle',
    error: undefined
}

//call API
export const fetchVideoData = createAsyncThunk('video/fetch', async(id:string|undefined) => {
    const response = await axios.get(`${baseUrlMovie}/${id}/videos?language=en-US`, {
        headers: {
            Authorization: `Bearer ${accessKey}`,
            'Content-Type': 'application/json;charset=utf-8',
        }
    })
    return response.data.results;
})

//create Slice
export const MovieDetailsTrailerPageSlice = createSlice({
    name: 'movieDetialsTrailerPage',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
        .addCase(fetchVideoData.pending, (state) => {
          state.status = "loading";
        })
        .addCase(fetchVideoData.fulfilled, (state, action) => {
          state.status = "succeeded";
          state.videos = action.payload;
        })
        .addCase(fetchVideoData.rejected,(state ,action) =>{
          state.status = "failed";
          state.error = action.error.message;
        }) 
    }
}) 

export default MovieDetailsTrailerPageSlice.reducer;
