import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { useFetchPage } from "../hooks/useFetchPage";
import { baseUrlTV } from "../../environment/environment";


export interface GenresTVPageState{
    id: number,
    name: string
}

export interface SeasonTVPageState {
    id: number,
    season_number: number,
    overview: string,
    poster_path: string,
    episode_count: number,
    air_date: string
}

export interface TVDetailsPageState {
    backdrop_path: string,
    genres: GenresTVPageState[],
    seasons: SeasonTVPageState[],
    vote_average: number,
    status: 'idle' | 'loading' | 'succeeded' | 'failed';
    error: string | undefined;
}


//initialState
const initialState : TVDetailsPageState = {
    backdrop_path: '',
    genres: [],
    seasons: [],
    vote_average: 0,
    status: 'idle',
    error: undefined
}

//call API
export const fetchDetailsTVPageData = createAsyncThunk('detailsTVPage/fetch', async(id:string|undefined) => {
    return useFetchPage(`${baseUrlTV}/${id}`);
})

//create Slice
export const tvDetailsPageSlice = createSlice({
    name: 'TVDetailsPage',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
        .addCase(fetchDetailsTVPageData.pending, (state) => {
          state.status = "loading";
        })
        .addCase(fetchDetailsTVPageData.fulfilled, (state, action) => {
          state.status = "succeeded";
          state.backdrop_path = action.payload.backdrop_path;
          state.genres = action.payload.genres;
          state.seasons = action.payload.seasons;
          state.vote_average = action.payload.vote_average;
        })
        .addCase(fetchDetailsTVPageData.rejected,(state ,action) =>{
          state.status = "failed";
          state.error = action.error.message;
        }) 
    }
})

export default tvDetailsPageSlice.reducer;