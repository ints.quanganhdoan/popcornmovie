import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { useFetchPage } from "../hooks/useFetchPage"
import { baseUrlMovie } from "../../environment/environment"

//define type
export interface GenresState{
    id: number,
    name: string
}

interface ProductionState{
    iso_3166_1: string,
    name: string
}

export interface MovieDetailsPageState {
    title: string,
    poster_path: string,
    release_date: string,
    overview: string,
    genres: GenresState[],
    production_countries: ProductionState[],
    vote_average: number,
    status: 'idle' | 'loading' | 'succeeded' | 'failed';
    error: string | undefined;
}

//initialState
const initialState : MovieDetailsPageState = {
    title: '',
    poster_path: '',
    release_date: '',
    overview: '',
    genres: [],
    production_countries: [],
    vote_average: 0,
    status: 'idle',
    error: undefined,
}

//create thunk
export const fetchDetailsMoviePageData = createAsyncThunk('detailMoviePage/fetch', async(id:string|undefined) => {
    return useFetchPage(`${baseUrlMovie}/${id}`);
})

//create slice
export const MovieDetailsPageSlice = createSlice({
    name: 'movieDetailsPage',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
        .addCase(fetchDetailsMoviePageData.pending, (state) => {
          state.status = "loading";
        })
        .addCase(fetchDetailsMoviePageData.fulfilled, (state, action) => {
          state.status = "succeeded";
          state.title = action.payload.title;
          state.poster_path = action.payload.poster_path;
          state.release_date = action.payload.release_date;
          state.overview = action.payload.overview;
          state.genres = action.payload.genres;
          state.production_countries = action.payload.production_countries;
          state.vote_average = action.payload.vote_average;
        })
        .addCase(fetchDetailsMoviePageData.rejected,(state ,action) =>{
          state.status = "failed";
          state.error = action.error.message;
        }) 
    },
})

export default MovieDetailsPageSlice.reducer;