import { accessKey, baseUrlTV } from "../../environment/environment";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axios from "axios";
import { SeasonDataType } from "./TVSeasonEpisodePageSlice";

//define type
export interface VideoType{
    id: string,
    key: string,
    name: string,
    type: string
}

interface VideoListType {
    videos: VideoType[],
    status: 'idle' | 'loading' | 'succeeded' | 'failed',
    error: string|undefined
}

//create initialState
const initialState : VideoListType = {
    videos: [],
    status: 'idle',
    error: undefined
}

//call API
export const fetchEpisodeVideoData = createAsyncThunk('videoEpisode/fetch', async({id, season_number, episode_number}:SeasonDataType) => {
    const response = await axios.get(`${baseUrlTV}/${id}/season/${season_number}/episode/${episode_number}/videos?language=en-US`, {
        headers: {
            Authorization: `Bearer ${accessKey}`,
            'Content-Type': 'application/json;charset=utf-8',
        }
    })
    return response.data.results;
})

//create Slice
export const EpisodeTrailerSlice = createSlice({
    name: 'episodeTrailer',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
        .addCase(fetchEpisodeVideoData.pending, (state) => {
          state.status = "loading";
        })
        .addCase(fetchEpisodeVideoData.fulfilled, (state, action) => {
          state.status = "succeeded";
          state.videos = action.payload;
        })
        .addCase(fetchEpisodeVideoData.rejected,(state ,action) =>{
          state.status = "failed";
          state.error = action.error.message;
        }) 
    }
}) 

export default EpisodeTrailerSlice.reducer;
