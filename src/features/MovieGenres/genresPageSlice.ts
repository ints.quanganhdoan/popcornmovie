import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlDiscover } from '../../environment/environment';
import { useFetchPage } from '../hooks/useFetchPage';

//initialState
export interface GenresPageItemState {
    id: number;
    poster_path: string;
    title: string;
    release_date: string;
}

export interface GenresPageState {
    page: number,
    movies: GenresPageItemState[],
    total_pages: number,
    status: 'idle' | 'loading' | 'succeeded' | 'failed';
    error: string | undefined;
}

interface FetchGenresPageParams {
  page: number;
  genresId: number;
}


export const initialState: GenresPageState = {
    page: 1,
    total_pages: 1,
    movies: [],
    status: 'idle',
    error: undefined,
};


//Call API
export const fetchGenresPageData = createAsyncThunk('genresPage/fetch', async ({page, genresId}:FetchGenresPageParams) => {
   return useFetchPage(`${baseUrlDiscover}?with_genres=${genresId}&page=${page}`);
});


//Slice
export const GenresPageSlice = createSlice({
   name: 'genresPage',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchGenresPageData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchGenresPageData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.page = action.payload.page;
       state.total_pages = action.payload.total_pages;
       state.movies = action.payload.results; 
     })
     .addCase(fetchGenresPageData.rejected,(state ,action) =>{
       state.status = "failed";
       state.error = action.error.message;
     })  
    }
});

export default GenresPageSlice.reducer;

