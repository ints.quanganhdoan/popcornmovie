import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { accessKey, baseUrlGenres } from "../../environment/environment";
import axios from "axios";

// define type
export interface GenresState {
    id: number,
    name: string
}

interface GenresListState {
    genres: GenresState[],
    status: 'idle' | 'loading' | 'succeeded' | 'failed';
    error: string | undefined;
}

// initialState
const initialState: GenresListState = {
    genres: [],
    status: 'idle',
    error: undefined
}

//call API
export const fetchGenresListData = createAsyncThunk('genresList/fetch', async() => {
    const response = await axios.get(`${baseUrlGenres}/list?language=en`, {
        headers:{
            Authorization: `Bearer ${accessKey}`,
            'Content-Type': 'application/json;charset=utf-8',
        }
    });

    return response.data.genres;
})

//create Slice
export const genresSlice = createSlice({
    name: 'genresList',
    initialState,
    reducers:{},
    extraReducers: (builder) => {
        builder
        .addCase(fetchGenresListData.pending, (state) => {
            state.status = "loading";
          })
          .addCase(fetchGenresListData.fulfilled, (state, action) => {
            state.status = "succeeded";
            state.genres = action.payload;
          })
          .addCase(fetchGenresListData.rejected,(state ,action) =>{
            state.status = "failed";
            state.error = action.error.message;
          })  
    }
})

export default genresSlice.reducer;