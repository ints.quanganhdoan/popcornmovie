import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { useFetchPage } from "../hooks/useFetchPage";
import { baseUrlSearch } from "../../environment/environment";

//define type
export interface MovieSearchState {
    id: number,
    poster_path: string,
    title?: string,
    name?:string,
    overview: string,
    vote_average: number,
    release_date: string
  }
  

export interface MovieSearchPageState {
    page: number,
    total_pages: number,
    movies: MovieSearchState[],
    query: string,
    status: 'idle' | 'loading' | 'succeeded' | 'failed',
    error: string | undefined;
}

//initialState
const initialState: MovieSearchPageState = {
    page: 1,
    total_pages: 1,
    movies: [],
    query: '',
    status: 'idle',
    error: undefined,
};

//call API
export const fetchSearchPageData = createAsyncThunk('searchPage/fetch', async({page, query}: {page: number, query:string}) => {
    return useFetchPage(`${baseUrlSearch}/multi?query=${query}&page=${page}`);
})

//create Slice
export const SearchPageSlice = createSlice({
    name: 'searchPage',
    initialState,
    reducers: {
        typeQuery: (state, action) => {
            state.query = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
        .addCase(fetchSearchPageData.pending, (state) => {
          state.status = "loading";
        })
        .addCase(fetchSearchPageData.fulfilled, (state, action) => {
          state.status = "succeeded";
          state.page = action.payload.page;
          state.total_pages = action.payload.total_pages;
          state.movies = action.payload.results; 
        })
        .addCase(fetchSearchPageData.rejected,(state ,action) =>{
          state.status = "failed";
          state.error = action.error.message;
        })  
    }
})

export default SearchPageSlice.reducer;
export const { typeQuery } = SearchPageSlice.actions;