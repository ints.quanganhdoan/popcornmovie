import axios from "axios";
import { accessKey } from "../../environment/environment";


//define type
export interface MovieState {
  id: number;
  poster_path: string;
  title: string;
}

export interface MovieListState {
  movies: MovieState[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | undefined;
}

export const useFetchMovieList = async (urlEndpoint:string) => {
    const response = await axios.get(urlEndpoint, {
      headers: {
        Authorization: `Bearer ${accessKey}`,
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
    
     return response.data.results;
};