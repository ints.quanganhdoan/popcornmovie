import axios from "axios";
import { accessKey } from "../../environment/environment";

//define type
export interface MoviePageItemState {
    id: number;
    poster_path: string;
    title: string;
    release_date: string;
}

export interface MoviePageState {
    page: number,
    movies: MoviePageItemState[],
    total_pages: number,
    status: 'idle' | 'loading' | 'succeeded' | 'failed';
    error: string | undefined;
}

export interface TVPageItemState {
  id: number;
  poster_path: string;
  name: string,
  origin_country: string;
}

export interface TVPageState {
  page: number,
  movies: TVPageItemState[],
  total_pages: number,
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | undefined;
}


export const useFetchPage = async (urlEndpoint:string) => {
    const response = await axios.get(urlEndpoint, {
      headers: {
        Authorization: `Bearer ${accessKey}`,
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
    
     return response.data;
};