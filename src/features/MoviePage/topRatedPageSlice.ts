import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlMovie } from '../../environment/environment';
import { useFetchPage } from '../hooks/useFetchPage';
import { MoviePageState } from '../hooks/useFetchPage';

//initialState
const initialState: MoviePageState = {
    page: 1,
    total_pages: 1,
    movies: [],
    status: 'idle',
    error: undefined,
};


//Call API
export const fetchTopRatedPageData = createAsyncThunk('topRatedPage/fetch', async (page:number) => {
   return useFetchPage(`${baseUrlMovie}/top_rated?language=en-US&page=${page}`);
  //  ${page}
});


//Slice
export const TopRatedPageSlice = createSlice({
   name: 'topRatedPage',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchTopRatedPageData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchTopRatedPageData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.page = action.payload.page;
       state.total_pages = action.payload.total_pages;
       state.movies = action.payload.results; 
     })
     .addCase(fetchTopRatedPageData.rejected,(state ,action) =>{
       state.status = "failed";
       state.error = action.error.message;
     })  
    }
});

export default TopRatedPageSlice.reducer;

