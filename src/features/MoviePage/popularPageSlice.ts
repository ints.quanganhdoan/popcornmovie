import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlMovie } from '../../environment/environment';
import { useFetchPage } from '../hooks/useFetchPage';
import { MoviePageState } from '../hooks/useFetchPage';

//initialState
const initialState: MoviePageState = {
    page: 1,
    total_pages: 1,
    movies: [],
    status: 'idle',
    error: undefined,
};


//Call API
export const fetchPopularPageData = createAsyncThunk('popularPage/fetch', async (page:number) => {
   return useFetchPage(`${baseUrlMovie}/popular?language=en-US&page=${page}`);
});


//Slice
export const PopularPageSlice = createSlice({
   name: 'popularPage',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchPopularPageData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchPopularPageData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.page = action.payload.page;
       state.total_pages = action.payload.total_pages;
       state.movies = action.payload.results; 
     })
     .addCase(fetchPopularPageData.rejected,(state ,action) =>{
       state.status = "failed";
       state.error = action.error.message;
     })  
    }
});

export default PopularPageSlice.reducer;

