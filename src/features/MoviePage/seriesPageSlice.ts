import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlTV } from '../../environment/environment';
import { TVPageState, useFetchPage } from '../hooks/useFetchPage';

//initialState
const initialState: TVPageState = {
    page: 1,
    total_pages: 1,
    movies: [],
    status: 'idle',
    error: undefined,
};


//Call API
export const fetchSeriesPageData = createAsyncThunk('seriesPage/fetch', async (page:number) => {
   return useFetchPage(`${baseUrlTV}/top_rated?language=en-US&page=${page}`);
});


//Slice
export const SeriesPageSlice = createSlice({
   name: 'seriesPage',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchSeriesPageData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchSeriesPageData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.page = action.payload.page;
       state.total_pages = action.payload.total_pages;
       state.movies = action.payload.results; 
     })
     .addCase(fetchSeriesPageData.rejected,(state ,action) =>{
       state.status = "failed";
       state.error = action.error.message;
     })  
    }
});

export default SeriesPageSlice.reducer;

