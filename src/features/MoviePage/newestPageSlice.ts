import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlMovie } from '../../environment/environment';
import { useFetchPage } from '../hooks/useFetchPage';
import { MoviePageState } from '../hooks/useFetchPage';

//initialState
const initialState: MoviePageState = {
    page: 1,
    total_pages: 1,
    movies: [],
    status: 'idle',
    error: undefined,
};


//Call API
export const fetchNewestPageData = createAsyncThunk('newestPage/fetch', async (page:number) => {
   return useFetchPage(`${baseUrlMovie}/now_playing?language=en-US&page=${page}`);
});


//Slice
export const NewestPageSlice = createSlice({
   name: 'newestPage',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchNewestPageData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchNewestPageData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.page = action.payload.page;
       state.total_pages = action.payload.total_pages;
       state.movies = action.payload.results; 
     })
     .addCase(fetchNewestPageData.rejected,(state ,action) =>{
       state.status = "failed";
       state.error = action.error.message;
     })  
    }
});

export default NewestPageSlice.reducer;

