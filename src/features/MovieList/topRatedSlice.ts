import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlMovie } from '../../environment/environment';
import {useFetchMovieList, MovieListState} from '../hooks/useFetchMovieList'


//initialState
const initialState: MovieListState = {
  movies: [],
  status: 'idle',
  error: undefined,
};


//Call API
export const fetchTopRatedListData = createAsyncThunk('topRatedList/fetch', async () => {
   return useFetchMovieList(`${baseUrlMovie}/top_rated?language=en-US&page=1`);
});


//Slice
export const topratedSlice = createSlice({
   name: 'topratedList',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchTopRatedListData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchTopRatedListData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.movies = action.payload;
     })
     .addCase(fetchTopRatedListData.rejected,(state ,action) =>{
       state.status = "failed";
       state.error = action.error.message;
     })  
    }
});

export default topratedSlice.reducer;

