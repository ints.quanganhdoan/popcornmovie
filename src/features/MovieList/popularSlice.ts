import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlMovie } from '../../environment/environment';
import {useFetchMovieList, MovieListState} from '../hooks/useFetchMovieList';

//initialState
const initialState: MovieListState = {
  movies: [],
  status: 'idle',
  error: undefined,
};


//Call API
export const fetchPopularListData = createAsyncThunk('popularList/fetch', async () => {
   return useFetchMovieList(`${baseUrlMovie}/popular?language=en-US&page=1`);
});


//Slice
export const popularSlice = createSlice({
   name: 'popularList',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchPopularListData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchPopularListData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.movies = action.payload;
     })
     .addCase(fetchPopularListData.rejected,(state ,action) =>{
       state.status ="failed";
       state.error= action.error.message;
     })  
    }
});

export default popularSlice.reducer;

