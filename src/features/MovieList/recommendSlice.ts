import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlMovie } from '../../environment/environment';
import {useFetchMovieList} from '../hooks/useFetchMovieList'

export interface MovieVerticalState {
  id: number,
  poster_path: string,
  title?: string,
  name?:string,
  overview: string,
  vote_average: number,
  release_date: string
}

export interface MovieVerticalListState {
  movies: MovieVerticalState[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | undefined;
}

//initialState
const initialState: MovieVerticalListState = {
  movies: [],
  status: 'idle',
  error: undefined,
};



//Call API
export const fetchRecommendListData = createAsyncThunk('recommendList/fetch', async () => {
  let movie_id;
  let result;
  do{
    movie_id = Math.floor(Math.random() * 957) + 62;
    try{
      result = await useFetchMovieList(`${baseUrlMovie}/${movie_id}/recommendations`);
    } 
    catch(err){
      console.log(`Failed to fetch recommendations for movie id ${movie_id}: ${err}`)
      result = null;
    }
  }
  while (!result || !result.length);
  return result;
});


//Slice
export const RecommendSlice = createSlice({
   name: 'RecommendList',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchRecommendListData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchRecommendListData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.movies = action.payload;
     })
     .addCase(fetchRecommendListData.rejected,(state ,action) =>{
       state.status = "failed";
       state.error = action.error.message;
     })  
    }
});

export default RecommendSlice.reducer;

