import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { baseUrlMovie } from '../../environment/environment';
import {useFetchMovieList, MovieListState} from '../hooks/useFetchMovieList'

//initialState
const initialState: MovieListState = {
  movies: [],
  status: 'idle',
  error: undefined,
};


//Call API
export const fetchNewestListData = createAsyncThunk('newestList/fetch', async () => {
   return useFetchMovieList(`${baseUrlMovie}/now_playing?language=en-US&page=1`);
});


//Slice
export const NewestSlice = createSlice({
   name: 'topratedList',
   initialState,
   reducers:{},
   extraReducers:(builder) => {
     builder
     .addCase(fetchNewestListData.pending, (state) => {
       state.status = "loading";
     })
     .addCase(fetchNewestListData.fulfilled, (state, action) => {
       state.status = "succeeded";
       state.movies = action.payload;
     })
     .addCase(fetchNewestListData.rejected,(state ,action) =>{
       state.status = "failed";
       state.error = action.error.message;
     })  
    }
});

export default NewestSlice.reducer;

