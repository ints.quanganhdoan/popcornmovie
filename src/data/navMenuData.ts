export const navMenuData = [
    {
        title: "Home",
        link: "/",
    },
    {
        title: "Coming Soon",
        link: "/coming_soon/1",
    },
    {
        title: "Series TV",
        link: "/series/1",
    },
    {
        title: "Genres",
        link: "/genres/Action/1",
    },
]