import { useNavigate } from 'react-router-dom';
import Button from '../components/common/Button';

const ErrorPage = () => {
    const navigate = useNavigate();

    return (
        <div className='w-screen h-screen flex flex-col justify-center items-center bg-backgroundError bg-cover text-white'>
            <h1 className='text-xl'>
                404
            </h1>
            <p className='text-xs'>
                Page not found
            </p>
            <Button
                className='bg-white text-black p-2 rounded-sm text-xs mt-3'
                onClick={() => {
                    sessionStorage.setItem('selectedItem', 'Home');
                    navigate('/');
                }}>
                Back to Homepage
            </Button>
        </div>
    );
};

export default ErrorPage;
