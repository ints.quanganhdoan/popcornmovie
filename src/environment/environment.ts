export const baseUrlMovie = 'https://api.themoviedb.org/3/movie';

export const baseUrlTV = 'https://api.themoviedb.org/3/tv';

export const baseUrlGenres = 'https://api.themoviedb.org/3/genre/movie';

export const baseUrlDiscover = 'https://api.themoviedb.org/3/discover/movie';

export const baseUrlSearch = 'https://api.themoviedb.org/3/search';

export const accessKey = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkNmVmNWYzYTZhN2Q2NDEwY2NjZGQ2NmNkNmI0ODI3ZiIsInN1YiI6IjY1MmM4ZGU1NzJjMTNlMDEwMzUxZTIwNyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.wT0pcGCPBrvp1IL3EK4wfYSYD1jNLydMlhaXbkj9Q3M';

