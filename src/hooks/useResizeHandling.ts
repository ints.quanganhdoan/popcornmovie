const useResizeHandling = (setIsOpen:(value:boolean) => void) => {
    if (window.innerWidth >= 1024) {
      setIsOpen(true);
    } else {
      setIsOpen(false);
    }
};

export default useResizeHandling;
