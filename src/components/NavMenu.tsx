import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import useResizeHandling from "../hooks/useResizeHandling";
import { routeToItem } from "../routes/route";

interface ItemType {
    title: string,
    link: string
}

interface NavMenuProps {
    data: ItemType[],
    isOpen: boolean,
    setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
}

const NavMenu = ({data, isOpen, setIsOpen}: NavMenuProps) => {
    
    const[selectedItem, setSelectedItem] = useState<string|null>(() => sessionStorage.getItem('selectedItem') || data[0].title);

    const handleResizeEvent = () => useResizeHandling(setIsOpen);

    const location = useLocation();

    useEffect(() => {
        for (const key in routeToItem) {
            if(location.pathname.includes(key)) {
                const selectedItem = routeToItem[key];
                setSelectedItem(selectedItem);
                break;
            }
            else {
                setSelectedItem('Home');
            }
        }
    },[location.pathname]);

    return (
        <ul className={`flex flex-col items-center ${isOpen ? 'block' : 'hidden'}
        lg:flex-row`}>
        {data.map((item: ItemType) => (
            <li key={item.title} className={`w-full text-center transition 
            ${
                selectedItem === item.title
                ? 'bg-white text-primaryColor lg:bg-[#222] lg:text-white'
                : 'lg:transition lg:hover:bg-white lg:hover:text-primaryColor lg:hover:opacity-75'
            } 
            ${selectedItem === item.title ? 'lg:hover:opacity-100 lg:hover:select-none' : ''}`}
            onClick={() => {
                setSelectedItem(item.title);
                handleResizeEvent;
            }}>
                <Link to={item.link} className='text-sm block w-full h-full p-2 lg:text-[15px]'>
                    {item.title}
                </Link>
            </li>
        ))}
        </ul>
    );
};

export default NavMenu;

