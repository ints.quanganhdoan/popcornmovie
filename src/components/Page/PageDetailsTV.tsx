import { useNavigate, useParams } from "react-router-dom"
import Button from "../common/Button"
import { IoCaretBackOutline } from "react-icons/io5";
import { RootState } from "../../store";
import { useAppDispatch, useAppSelector } from "../../features/hooks/hooks";
import { useEffect } from "react";
import { fetchDetailsTVPageData } from "../../features/DetailsPage/TVDetailsPageSlice";
import TVDetailsInfor from "../TVDetails/TVDetailsInfor";
import TVDetailsSeason from "../TVDetails/TVDetailsSeason";


const PageDetailsTV = () => {

    const navigate = useNavigate();
    const {id, name} = useParams();
    
    const detailsTV = useAppSelector((state: RootState) => state.tvDetailsPage);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchDetailsTVPageData(id));
    },[id]);

    return (
        <div className="container">
            <div className=" flex flex-col items-center">
                <div className="flex justify-center items-center mt-2 w-full relative">
                    <Button
                    onClick={() => navigate(-1)} 
                    className='absolute left-0 text-xl lg:hidden'>
                        <IoCaretBackOutline />
                    </Button>
                    <h1 className="text-center font-bold text-[15px] lg:text-base">{name}</h1>
                </div>
                <div className="flex flex-col mt-3 gap-x-5 w-full ">
                <TVDetailsInfor detailsTV={detailsTV}/>
                <TVDetailsSeason detailsTV={detailsTV}/>
                </div>
            </div>
        </div>
    );
};

export default PageDetailsTV;