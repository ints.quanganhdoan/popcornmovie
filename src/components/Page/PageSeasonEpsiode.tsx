import { useNavigate, useParams } from "react-router-dom";
import Button from "../common/Button";
import { IoCaretBackOutline } from "react-icons/io5";
import { useAppDispatch, useAppSelector } from "../../features/hooks/hooks";
import { RootState } from "../../store";
import { useEffect, useState } from "react";
import { fetchDetailsTVPageData } from "../../features/DetailsPage/TVDetailsPageSlice";
import { fetchTVSeasonEpisodeData } from "../../features/DetailsPage/TVSeasonEpisodePageSlice";
import Video from "../MovieDetails/Video";
import { fetchEpisodeVideoData } from "../../features/DetailsPage/TVSeasonEpisodeTrailerSlice";
import EpisodeInfor from "../TVDetails/EpisodeInfor";
import EpisodePagination from "../TVDetails/EpisodePagination";

const PageSeasonEpsiode = () => {
    const navigate = useNavigate();
    const[episodeNumber, setEpisodeNumber] = useState<number|undefined>(1);

    const {name, season_number, id} = useParams(); 
    const seasonNumber = parseInt(season_number as string);

    const seasons = useAppSelector((state:RootState) => state.tvDetailsPage.seasons);
    const seasonEpisode = useAppSelector((state:RootState) => state.tvSeasonEpisode);
    const seasonEpisodeTrailer = useAppSelector((state:RootState) => state.tvEpisodeTrailer);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchDetailsTVPageData(id));
    },[id]);

    useEffect(() => {
        dispatch(fetchTVSeasonEpisodeData({id, season_number, episode_number: episodeNumber?.toString()}));
    },[id, season_number, episodeNumber])

    useEffect(() => {
        dispatch(fetchEpisodeVideoData({id, season_number, episode_number: episodeNumber?.toString()}));
    },[id, season_number, episodeNumber])
    
    const currentSeason = seasons.find(season => season.season_number === seasonNumber);
    const episodeCount = currentSeason ? currentSeason.episode_count : 0;

    const priorityList:string[] = ['Trailer', 'Teaser', 'Clip', 'Behind the Scenes', 'Bloopers', 'Featurette', 'Opening Credits'];

    return (
        <div className="container">
            <div className=" flex flex-col items-center">
                <div className="flex justify-center items-center mt-2 w-full relative">
                    <Button
                    onClick={() => navigate(-1)} 
                    className='absolute left-0 text-xl lg:hidden'>
                        <IoCaretBackOutline />
                    </Button>
                    <div className="text-center font-bold text-[15px]">
                        <span className="text-primaryColor">{name}</span><br /> 
                        {seasonNumber === 0 ? 'Special Season' : `Season ${seasonNumber}`}
                    </div>
                </div>
                <div className="mt-5 w-full">
                    <EpisodeInfor list={seasonEpisode}/>
                </div>
                <div className="flex w-full justify-center h-56 lg:h-[80vh]">
                    <Video videoList={seasonEpisodeTrailer.videos} priorityList={priorityList}/>
                </div>
                <EpisodePagination totalEpisode={episodeCount} episodeNumber={episodeNumber as number} setEpisodeNumber={setEpisodeNumber}/>
            </div>
        </div>
    );
};

export default PageSeasonEpsiode;