import { useNavigate, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../features/hooks/hooks";
import { RootState } from "../../store";
import { useEffect, useState} from "react";
import { fetchDetailsMoviePageData } from "../../features/DetailsPage/MovieDetailsPageSlice";
import { fetchVideoData } from "../../features/DetailsPage/MovieDetailsTrailerSlice";
import Video from "../MovieDetails/Video";
import DetailsInfor from "../MovieDetails/DetailsInfor";
import Summary from "../MovieDetails/Summary";
import Button from "../common/Button";
import {IoCaretBackOutline} from 'react-icons/io5'
import { fetchReviewsData } from "../../features/DetailsPage/MovieDetailsReviewSlice";
import Review from "../MovieDetails/Review";

const PageDetailsMovie = () => {
    const { id } = useParams();
    const navigate = useNavigate();

    const detailsMovie = useAppSelector((state:RootState) => state.movieDetailsPage);
    const trailerMovie = useAppSelector((state:RootState) => state.movieDetailsTrailer);
    const reviewMovie = useAppSelector((state: RootState) => state.movieDetailsReview);
    const dispatch = useAppDispatch();

    const[page, setPage] = useState(1);

    useEffect(() => {
        dispatch(fetchDetailsMoviePageData(id));
    },[id]);

    useEffect(() => {
        dispatch(fetchVideoData(id));
    },[id]);

    useEffect(() => {
        dispatch(fetchReviewsData({id : id, page : page}));
    },[id]);

    const priorityList:string[] = ['Trailer', 'Teaser', 'Clip', 'Behind the Scenes', 'Bloopers', 'Featurette', 'Opening Credits'];

    const[isOpen, setIsOpen] = useState(false);

    
    return (
       <div className="container">
            <div className=" flex flex-col items-center">
                <div className="flex justify-center items-center mt-2 w-full relative">
                    <Button
                    onClick={() => navigate(-1)} 
                    className='absolute left-0 text-xl lg:hidden'>
                        <IoCaretBackOutline />
                    </Button>
                    <h1 className="text-center font-bold text-[15px] lg:text-base">{detailsMovie.title}</h1>
                </div>
                <div className="flex w-full justify-center h-56 lg:h-[80vh]">
                    <Video 
                    videoList={trailerMovie.videos} 
                    priorityList={priorityList} />
                </div>
                <DetailsInfor detailsPage={detailsMovie}/>
                <Summary 
                detailsPage={detailsMovie} 
                className="mt-[14px] p-3 w-full bg-primaryColor text-white rounded-sm lg:hidden"
                />
                <div className='mt-2 w-full'>
                    <Button 
                    className='w-full bg-white rounded-sm border border-primaryColor text-primaryColor p-1 text-sm hover:text-white hover:bg-primaryColor'
                    onClick={() => setIsOpen(!isOpen)}
                    >
                        Reviews
                    </Button>
                    <Review isOpen={isOpen} list={reviewMovie} setPage={setPage}/>
                </div>
            </div>
        </div>
    );
};

export default PageDetailsMovie;