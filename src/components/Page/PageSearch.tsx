import { useEffect} from "react";
import { useAppDispatch, useAppSelector } from "../../features/hooks/hooks";
import ListMovie from "../ListMovie";
import { fetchSearchPageData, typeQuery } from "../../features/MovieSearch/searchPageSlice";
import Pagination from "../common/Pagination";
import { RootState } from "../../store";
import { useNavigate, useParams } from "react-router-dom";

const PageSearch = () => {

    const searchList = useAppSelector((state:RootState) => state.searchPage);
    const query = useAppSelector((state:RootState) => state.searchPage.query);
    const dispatch = useAppDispatch();

    const { page } = useParams();
    const currentPage = page ? parseInt(page) : 1;

    const navigate = useNavigate();

    const setPage = (newPage:number) => {
        navigate(`/search/${query}/${newPage}`);
    }

    useEffect(() => {
        if(sessionStorage.getItem('search_string')){
            dispatch(typeQuery(sessionStorage.getItem('search_string')));
        }
    },[])

    useEffect(() => {
        dispatch(fetchSearchPageData({page: currentPage, query}));
    },[currentPage, query])

    
    return (
        <div className="container">
            <h1 className="text-center mt-3">
                Result for <span className="font-semibold">"{query}"</span>
            </h1>
            <ListMovie list={searchList.movies} />
            <Pagination list={searchList} setPage={setPage}/>
        </div>
    );
};

export default PageSearch;