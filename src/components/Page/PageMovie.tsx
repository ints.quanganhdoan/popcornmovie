import React from 'react';
import { MoviePageState } from '../../features/hooks/useFetchPage';
import { Link } from 'react-router-dom';
import Loading from '../common/Loading';
import useImageErrorHandling from '../../hooks/useImageErrorHandling';
import { GenresPageState } from '../../features/MovieGenres/genresPageSlice';

const PageMovie = ({list} : React.PropsWithChildren<{ list: MoviePageState|GenresPageState}>) => {
    if (list.movies.length === 0){
        return(
            <Loading className='text-center'>Loading...</Loading>
        )
    }

    const handleImageError = useImageErrorHandling('/Error/image_not_found.png')

    return (
        <div className='flex justify-center'>
            <div 
            className="grid grid-cols-2 gap-3 justify-start
            sm:gap-4
            md:grid-cols-4 md:gap-3
            lg:gap-5
            xl:grid-cols-5"
            >
            {
                list.movies.map((item) => (
                    <Link to={`/movie_details/${item.id}`} key={item.id}>
                        <div className="flex flex-col justify-center items-center w-40 gap-y-1 hover:cursor-pointer hover-group
                        sm:w-[170px]
                        lg:w-44
                        xl:w-48"
                        >
                            <div className='overflow-hidden w-full'>
                                <img 
                                src={`https://image.tmdb.org/t/p/original${item.poster_path}`} 
                                alt={item.title}
                                className="h-60 w-auto hoverImg
                                sm:h-64 sm:w-full
                                xl:h-72"
                                onError={handleImageError}
                                />
                            </div>
                            <div className="text-center text-sm truncate w-full text-blue-600">
                                {item.title}
                            </div>
                            <div className="text-center text-sm truncate w-full text-gray-600">
                                {item.release_date}
                            </div>
                        </div>
                    </Link>
                ))
            }
            </div>
        </div>
    );
};

export default PageMovie;