import { FaSearch } from 'react-icons/fa';
import Button from './common/Button';
import { useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { typeQuery } from '../features/MovieSearch/searchPageSlice';

const SearchInput = () => {
    const dispatch = useDispatch();

    const searchRef = useRef<HTMLInputElement>(null);
    const[inputSearch, setInputSearch] = useState<string>('');
    const navigate = useNavigate();

    const handleSearch = () => {
        sessionStorage.setItem('search_string', searchRef.current?.value as string);
        searchRef.current!.value = '';
        dispatch(typeQuery(sessionStorage.getItem('search_string')));
        navigate(`/search/${inputSearch}/1`)
    }

    return (
        <section className='relative'>
            <input
            placeholder='Tìm kiếm...'
            className='pl-2 py-2 pr-8 w-full border border-primaryColor text-sm lg:border-t-transparent'
            ref={searchRef}
            onChange={(e) => setInputSearch(e.target.value)}
            onKeyUp={(e) => {
                if(e.key==='Enter' && document.activeElement === e.target){
                    if(searchRef.current?.value===''){
                        //do nothing
                    }
                    else handleSearch();
                }
            }}>
            </input>
            <Button 
            className={`absolute top-1/4 right-2 text-gray-500 ${inputSearch.length > 0 ? 'text-primaryColor' : ''} hover:text-blue-400`}
            onClick={() => {
                if(searchRef.current?.value===''){
                    //do nothing
                }
                else handleSearch();
            }}
            >
                <FaSearch />
            </Button>
        </section>
    );
};

export default SearchInput;