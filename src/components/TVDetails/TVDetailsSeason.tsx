import { Link, useParams } from 'react-router-dom';
import { TVDetailsPageState } from '../../features/DetailsPage/TVDetailsPageSlice';
import useImageErrorHandling from '../../hooks/useImageErrorHandling';

const TVDetailsSeason = ({detailsTV}  : {detailsTV: TVDetailsPageState}) => {

    const {id, name} = useParams();

    const handleImageError = useImageErrorHandling('/Error/image_not_found.png');

    return (
        <div>
            {
            detailsTV?.seasons.map((item) => (
                <Link to={`/tv_details/${id}/${name}/season/${item.season_number}`} key={item.id}>
                    <div className="flex transition hover:translate-x-5 group-gray-scale">
                        <img 
                        src={`https://image.tmdb.org/t/p/original${item.poster_path}`} 
                        alt="poster_path"
                        onError={handleImageError}
                        className="w-36 h-52 pl-0 pr-2 py-2 hover-gray-scale-item"
                        />
                        <div className="flex flex-1 flex-col-space-between p-2 w-full text-sm min-w-0">
                            <div>
                                <p className='font-medium'>
                                    {item.season_number === 0 ? 'Special Season' : `Season ${item.season_number}`}
                                </p>
                                <p className='text-xs text-gray-500'>{item.air_date}</p>
                            </div>
                            <div className="multi-line-truncate">
                                {item.overview}
                            </div>
                            <p>
                                Episodes: {item.episode_count}
                            </p>
                        </div>
                    </div>
                </Link>
            ))
            }
        </div>
    );
};

export default TVDetailsSeason;