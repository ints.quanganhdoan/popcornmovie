import StarRatings from 'react-star-ratings';
import useImageErrorHandling from '../../hooks/useImageErrorHandling';
import { SeasonPageState } from '../../features/DetailsPage/TVSeasonEpisodePageSlice';
import EpisodeSummary from './EpisodeSummary';

const EpisodeInfor = ({list} : {list: SeasonPageState}) => {

    const handleImageError = useImageErrorHandling('/Error/series_tv_not_found_img.png')

    return (
        <div>
            <div className="flex gap-x-3">
                <img 
                src={`https://image.tmdb.org/t/p/original${list.still_path}`} 
                alt="season poster"
                onError={handleImageError}
                className="w-44 h-28
                lg:w-56 lg:h-auto" 
                />
                <div className="flex flex-col text-sm w-full">
                    <p className="font-medium">{list.name}</p>
                    <p className="text-xs text-gray-500">{list.air_date}</p>
                    <div>
                        <StarRatings 
                        rating={list.vote_average*0.5}
                        starDimension="14px"
                        starSpacing="5px"
                        starRatedColor="#8E182E"
                        />
                    </div>
                    <EpisodeSummary list={list} className='hidden lg:block' />
                </div>
            </div>
            <EpisodeSummary list={list} className='lg:hidden' />
        </div>
    );
};

export default EpisodeInfor;