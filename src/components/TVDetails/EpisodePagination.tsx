import Button from '../common/Button';

interface EpisodePaginationType {
    totalEpisode: number,
    episodeNumber: number,
    setEpisodeNumber: (episodeNumber: number) => void
}

const EpisodePagination = ({totalEpisode, episodeNumber, setEpisodeNumber} : EpisodePaginationType) => {
    return (
        <div className="flex flex-wrap gap-x-2 gap-y-2 mt-7">
        {
            [...Array(totalEpisode)].map((_ ,index) => (
                <Button
                className={` w-24 h-9 ${episodeNumber === (index+1) ? 'text-primaryColor bg-white border border-primaryColor' : 'text-white bg-primaryColor'} rounded-sm`} 
                key={index}
                onClick={() => setEpisodeNumber(index+1)}
                >
                    Episode {index + 1}
                </Button>
            ))
        }
    </div>
    );
};

export default EpisodePagination;