import { SeasonPageState } from '../../features/DetailsPage/TVSeasonEpisodePageSlice';

const EpisodeSummary = ({list, className} : {list:SeasonPageState, className: string}) => {
    return (
        <div className={`bg-primaryColor text-white p-2 mt-3 rounded-sm flex-grow ${className}`}>
            <p className="font-medium">Summary</p>
            <p className="text-justify text-sm">
                {list.overview}
            </p>   
        </div>
    );
};

export default EpisodeSummary;