import StarRatings from 'react-star-ratings';
import useImageErrorHandling from '../../hooks/useImageErrorHandling';
import { TVDetailsPageState } from '../../features/DetailsPage/TVDetailsPageSlice';

const TVDetailsInfor = ({detailsTV} : {detailsTV:TVDetailsPageState}) => {

    const handleImageError = useImageErrorHandling('/Error/image_not_found.png');

    return (
        <div>
            <img 
            src={`https://image.tmdb.org/t/p/original${detailsTV.backdrop_path}`}
            alt="image" 
            className="w-full"
            onError={handleImageError}
            />
            <div>
                    <span className="text-sm mr-2">Genres:</span>
                    {
                        detailsTV.genres.map((item, index) => (
                            <span key={item.id} className="text-xs">
                            {item.name}
                            {index < detailsTV.genres.length - 1 ? ',\u00A0' : ''}
                            </span>
                        ))
                    }
            </div>
            <div>
                <span className="text-sm mr-2">Rating:</span>
                <StarRatings 
                rating={detailsTV.vote_average*0.5}
                starDimension="12px"
                starSpacing="5px"
                starRatedColor="#8E182E"
                />
            </div>
        </div>
    );
};

export default TVDetailsInfor