import React, { useRef, useState } from 'react';
import Button from '../common/Button';
import useImageErrorHandling from '../../hooks/useImageErrorHandling';
import { ReviewPageState } from '../../features/DetailsPage/MovieDetailsReviewSlice';
import Pagination from '../common/Pagination';
import { ImUpload } from 'react-icons/im'

interface ReviewListType {
    isOpen: boolean,
    list: ReviewPageState,
    setPage: React.Dispatch<React.SetStateAction<number>>
}

const Review = ({isOpen, list, setPage} : ReviewListType) => {

    const handleErrorImg = useImageErrorHandling('/Error/avatar_default.png');
    const [imgSrc, setImgSrc] = useState<string>('');
    const fileInput = useRef<HTMLInputElement>(null);
    
    const [expandedReviews, setExpandedReviews] = useState<boolean[]>(
        new Array(list.reviews.length).fill(false)
    );

    const toggleReviewExpansion = (index: number) => {
        const updatedExpandedReviews = [...expandedReviews];
        updatedExpandedReviews[index] = !updatedExpandedReviews[index];
        setExpandedReviews(updatedExpandedReviews);
    };

    const handleImageClick = () => {
        fileInput.current?.click();
    }

    const handleFileChange = (e : React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        const reader = new FileReader();
        reader.onloadend = () => {
            setImgSrc(reader.result as string);
        }
        if (file) {
            reader.readAsDataURL(file);
        } else {
            setImgSrc("");
        }
    }
    
    return (
        <div className={`${isOpen? '' : 'hidden'}`}>
            <form className="border border-primaryColor p-2 mt-2">
                <div className="flex items-center gap-x-3">
                    <div 
                    className='relative hover-group-avatar hover:cursor-pointer'
                    onClick={handleImageClick}
                    >
                        <img 
                        src={imgSrc} 
                        alt="avatar"
                        className="w-10 h-10 rounded-full hover-avatar"
                        onError={handleErrorImg} 
                        />
                        <ImUpload className='absolute top-3 left-[10px] text-xl hidden hover-upload'/>
                        <input 
                        type="file"
                        ref={fileInput}
                        style={{display: "none"}}
                        onChange={handleFileChange}
                        />
                    </div>
                    <input 
                    type="text" 
                    placeholder="Yourname ..."
                    className="text-sm"
                    />
                </div>
                <textarea placeholder="Comment ..." className="my-2 h-16 w-full p-1 text-sm"/>
                <Button 
                className='p-2 bg-primaryColor text-sm text-white rounded-sm'
                type="reset"
                >
                    Submit
                </Button>
            </form>
            {
                list.reviews.length > 0 ? list.reviews.map((item, index) => (
                    <div key={index} className="p-2">
                        <div className="flex items-center gap-x-3">
                            <img 
                            src={`https://image.tmdb.org/t/p/original${item.author_details.avatar_path}`}
                            alt="avatar" 
                            className="w-10 h-10 rounded-full"
                            onError={handleErrorImg}
                            />
                            <p className="text-sm">{item.author}</p>
                        </div>
                        <div className={`text-sm p-1`}>
                            {expandedReviews[index] ? item.content : item.content.slice(0, 250)}
                            {item.content.length > 250 && (
                                <p className='text-blue-500 cursor-pointer' onClick={() => toggleReviewExpansion(index)}>
                                    {expandedReviews[index] ? 'Collapsed' : 'More'}
                                </p>
                            )}
                        </div>
                    </div>
                ))
                :
                <div className="text-center">No reviews were found</div>
            }
            {list.reviews.length > 0 && <Pagination list={list} setPage={setPage}/>}
        </div>
    );
};

export default Review;