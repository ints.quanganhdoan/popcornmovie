import { MovieDetailsPageState } from "../../features/DetailsPage/MovieDetailsPageSlice";

const Summary = ({detailsPage, className}: {detailsPage:MovieDetailsPageState, className?: string}) => {
    return (
        <div className={`${className}`}>
            <p className="font-medium">Summary</p>
            <p className="text-justify text-sm">
                {detailsPage.overview}
            </p>      
        </div>
    );
};

export default Summary;