import ReactPlayer from "react-player";
import { VideoType } from "../../features/DetailsPage/MovieDetailsTrailerSlice";


const getVideoKey = ({videoList, priorityList} : {videoList:VideoType[], priorityList:string[]|undefined}) => {
    for (const type of priorityList as string[]) {
      const video : VideoType | undefined = videoList.find((item:VideoType) => item.type === type);
      if (video) {
        return video.key; 
      }
    }
    return null; 
};

const Video = ({videoList, priorityList}:{videoList: VideoType[], priorityList?: string[]}) => {

  const videoKey = getVideoKey({ videoList: videoList, priorityList:priorityList })

  return (
      <div className="flex mt-3 w-full justify-center h-56
      sm:h-72
      md:h-96
      lg:h-full">
        {
          videoKey !== null ? 
          <ReactPlayer 
          url={`https://www.youtube.com/embed/${videoKey}`}
          width={'100%'}
          height={'100%'}
          controls={true}
          playing={true}
          />
          :
          <img src="/Error/video_not_found.jpg" alt="error not found image" className="w-full"/>
        }
      </div>
  );
};

export default Video;