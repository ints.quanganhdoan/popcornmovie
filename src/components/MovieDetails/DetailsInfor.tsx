import { FlagIcon, FlagIconCode } from "react-flag-kit";
import { MovieDetailsPageState } from "../../features/DetailsPage/MovieDetailsPageSlice";
import useImageErrorHandling from "../../hooks/useImageErrorHandling";
import StarRatings from "react-star-ratings";
import Summary from "./Summary";


const DetailsInfor = ({detailsPage} : {detailsPage: MovieDetailsPageState}) => {

    const handleImageError = useImageErrorHandling('/Error/image_not_found.png');

    return (
        <div className="flex mt-[22px] gap-x-5 w-full
        sm:mt-[88px]
        md:mt-[183px]
        lg:mt-[22px]">
            <img 
            src={`https://image.tmdb.org/t/p/original${detailsPage.poster_path}`} 
            alt="image" 
            className="w-28 h-44 shadow-md shadow-black
            lg:w-52 lg:h-auto"
            onError={handleImageError}
            />
            <div className="flex flex-col justify-center gap-y-2 w-full
            lg:text-base lg:justify-between">
                <div className="flex flex-col gap-y-1">
                    <p className="text-sm font-semibold">
                        {detailsPage.title}
                    </p>
                    <p  className="text-xs text-gray-500">
                        {detailsPage.release_date}
                    </p>
                </div>
                <div className="flex flex-col flex-wrap text-sm gap-y-1
                sm:flex-row sm:items-center"> 
                    <p className="text-sm italic
                    sm:mr-2">
                        Genres:
                    </p>
                    <div className="flex flex-wrap">
                        {detailsPage.genres.map((item, index) => (
                            <span key={item.id} className="text-xs sm:text-sm">
                                {item.name}
                                {index < detailsPage.genres.length - 1 ? ',\u00A0' : ''}
                            </span>
                        ))}
                    </div>
                </div>
                <div className="flex items-center gap-x-3">
                    <p className="text-sm italic">National:</p>
                    {detailsPage.production_countries.map((item) => (
                            <span key={item.iso_3166_1}>
                                <FlagIcon code={item.iso_3166_1 as FlagIconCode} size={18}/>
                            </span>
                    ))}
                </div>
                <div>
                    <StarRatings 
                    rating={detailsPage.vote_average*0.5}
                    starDimension="14px"
                    starSpacing="5px"
                    starRatedColor="#8E182E"
                    />
                </div>
                <Summary 
                detailsPage={detailsPage} 
                className="mt-[14px] p-3 w-full bg-primaryColor text-white rounded-sm hidden
                lg:block"
                />
            </div>
        </div>
    );
};

export default DetailsInfor;