import { useState } from "react";
import { GenresState } from "../features/MovieGenres/genresSlice";
import Button from "./common/Button";
import { AiFillCaretDown, AiFillCaretRight } from "react-icons/ai";


interface genresListType {
    active: string,
    list: GenresState[],
    onClick: (name:string, id:number) => void
}


const ListGenres = ({active, list, onClick}:genresListType) => {
    
    const[show, setShow] = useState<boolean>(false);

    return (
        <>
            <div 
            className="flex items-center justify-center gap-x-1 text-primaryColor font-semibold mb-3 select-none"
            onClick={() => setShow(!show)}
            >
                    {show ? <AiFillCaretDown /> : <AiFillCaretRight />}
                    Genres
            </div>
            <div className={`flex justify-center ${show ? '' : 'hidden'}`}>
                <div 
                className="grid grid-cols-2 gap-3 mb-4 justify-start
                md:grid-cols-4
                lg:grid-cols-5
                xl:grid-cols-7
                ">
                    {list?.map((item) => (
                        <Button 
                        key={item.id}
                        className={`text-center text-sm p-2 rounded-md w-32 ${active === item.name ? 'hoverBtn' : 'btn'}`}
                        onClick={() => {
                            onClick(item.name, item.id);
                            setShow(!show);
                        }}
                        >
                            {item.name}
                        </Button>
                    ))}
                </div>
            </div>
        </>
    );
};

export default ListGenres;