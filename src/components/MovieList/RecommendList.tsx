import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../features/hooks/hooks";
import { fetchRecommendListData } from "../../features/MovieList/recommendSlice";
import ListMovie from "../ListMovie";
import { RootState } from "../../store";


const RecommendList = () => {

    const dispatch = useAppDispatch();
    const recommendList = useAppSelector((state:RootState) => state.recommendList);

    useEffect(() => {
        dispatch(fetchRecommendListData());
    },[dispatch]);

    return (
        <div className="bg-primaryColor mt-3">
            <h1 
            className="text-center text-white text-sm font-semibold p-3 -mb-3 
            lg:text-base">
                Recommend For You
            </h1>
            <ListMovie list={recommendList.movies} textColor="text-white"/>
        </div>
    );
};

export default RecommendList;