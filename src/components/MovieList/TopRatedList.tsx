import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { fetchTopRatedListData } from '../../features/MovieList/topRatedSlice';
import { useAppDispatch, useAppSelector } from '../../features/hooks/hooks';
import Carousel from '../common/Carousel';
import { RootState } from '../../store';


const TopRatedList = () => {
    
    const dispatch = useAppDispatch();
    const topRatedList = useAppSelector((state:RootState) => state.topRatedList);
    
    useEffect(() => {
        dispatch(fetchTopRatedListData());
    },[dispatch]);

    return (
        <div className='flex flex-col mt-2'>
            <div className='flex justify-between'>
                <h1 className='text-center text-sm text-primaryColor font-semibold
                lg:text-base'>
                    Top Rated
                </h1>
                <Link to="/home/top_rated/1" className='text-sm underline underline-offset-4 hover:text-blue-400
                lg:text-base'>
                    See more
                </Link>
            </div>
                <Carousel list={topRatedList.movies}/>
        </div>
    );
};

export default TopRatedList;