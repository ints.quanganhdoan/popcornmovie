import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { fetchNewestListData } from '../../features/MovieList/newestSlice';
import { useAppDispatch, useAppSelector } from '../../features/hooks/hooks';
import Carousel from '../common/Carousel';
import { RootState } from '../../store';


const NewestList = () => {
    
    const dispatch = useAppDispatch();
    const newestList = useAppSelector((state: RootState) => state.newestList);
    
    useEffect(() => {
        dispatch(fetchNewestListData());
    },[dispatch]);

    return (
        <div className='flex flex-col mt-2'>
            <div className='flex justify-between'>
                <h1 className='text-center text-sm text-primaryColor font-semibold
                lg:text-base'>
                    Newest
                </h1>
                <Link to="/home/newest/1" className='text-sm underline underline-offset-4 hover:text-blue-400
                lg:text-base'>
                    See more
                </Link>
            </div>
                <Carousel list={newestList.movies}/>
        </div>
    );
};

export default NewestList;