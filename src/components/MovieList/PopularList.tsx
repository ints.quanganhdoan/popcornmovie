import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { fetchPopularListData } from '../../features/MovieList/popularSlice';
import { useAppDispatch, useAppSelector } from '../../features/hooks/hooks';
import Carousel from '../common/Carousel';
import { RootState } from '../../store';


const PopularList = () => {
    
    const dispatch = useAppDispatch();
    const popularList = useAppSelector((state:RootState) => state.popularList);
    
    useEffect(() => {
        dispatch(fetchPopularListData());
    },[dispatch]);

    return (
        <div className='flex flex-col mt-2'>
            <div className='flex justify-between'>
                <h1 className='text-center text-sm text-primaryColor font-semibold
                lg:text-base'>
                    Popular
                </h1>
                <Link to="/home/popular/1" className='text-sm underline underline-offset-4 hover:text-blue-400
                lg:text-base'>
                    See more
                </Link>
            </div>
                <Carousel list={popularList.movies}/>
        </div>
    );
};

export default PopularList;