import Button from './Button';
import { MoviePageState, TVPageState } from '../../features/hooks/useFetchPage';
import { useEffect, useState } from 'react';
import { FaBackwardFast, FaBackwardStep, FaForwardFast, FaForwardStep } from 'react-icons/fa6';
import { GenresPageState } from '../../features/MovieGenres/genresPageSlice';
import { MovieSearchPageState } from '../../features/MovieSearch/searchPageSlice';
import { ReviewPageState } from '../../features/DetailsPage/MovieDetailsReviewSlice';

const Pagination = ({list, setPage} : 
    React.PropsWithChildren<{list:MoviePageState|TVPageState|GenresPageState|MovieSearchPageState|ReviewPageState, setPage:(page:number) => void}>) => {

    const [startPage, setStartPage] = useState<number>(1);

    useEffect(() => {
        if(list.page === list.total_pages){
            setStartPage(Math.max(list.page - 2, 1))
        }
        else if (list.page < startPage || list.page > startPage + 2) {
            setStartPage(Math.max(list.page - 1, 1));
        }
    }, [list.page]);

    return (
        <div className="flex justify-center gap-x-2 mt-4" key={startPage}>
            <Button 
            className={`w-8 h-8 text-sm ${list.page===1 ? 'notAllowedBtn' : 'btn'}
            lg:w-10 lg:h-10 lg:text-base`}
            onClick={() => {
                setStartPage(1);
                setPage(1);
            }}
            disabled={list.page===1 ? true : false}  
            >
                <FaBackwardFast />
            </Button>
            <Button 
            className={`w-8 h-8 text-sm ${list.page===1 ? 'notAllowedBtn' : 'btn'}
            lg:w-10 lg:h-10 lg:text-base`}
            onClick={() => {
                setPage(list.page-1);
            }}
            disabled={list.page===1 ? true : false}  
            >
                <FaBackwardStep />
            </Button>
            {[...Array(list.total_pages)].slice(0, 3).map((index, item) => (
                <Button 
                key={index} 
                className={`
                ${
                    item+startPage === list.page ? 
                    'hoverBtn' : 'btn'
                }
                w-8 h-8 text-sm hover:hoverBtn
                lg:w-10 lg:h-10 lg:text-base`}
                onClick={() => {
                    setPage(item+startPage);
                }}
                >
                    {item + startPage}
                </Button>
            ))}
            <Button 
            className={`w-8 h-8 text-sm ${list.page===list.total_pages || list.page === 500 ? 'notAllowedBtn' : 'btn'}
            lg:w-10 lg:h-10 lg:text-base`}
            onClick={() => {
                setPage(list.page+1)
            }}
            disabled={list.page===list.total_pages || list.page === 500 ? true : false}  
            >
                <FaForwardStep />
            </Button>
            <Button 
            className={`w-8 h-8 text-sm ${list.page===list.total_pages || list.page === 500 ? 'notAllowedBtn' : 'btn'}
            lg:w-10 lg:h-10 lg:text-base`}
            onClick={() => {
                if(list.total_pages <= 500) {
                    const newStart = list.total_pages-3;
                    setPage(newStart+3)
                }
                else {
                    const newStart = 500-2;
                    setStartPage(newStart);
                    setPage(500);
                }
            }}
            disabled={list.page===list.total_pages || list.page === 500 ? true : false} 
            >
                <FaForwardFast />
            </Button>
        </div>
    );
};

export default Pagination;