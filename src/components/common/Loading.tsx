import React, { ReactNode } from "react";

interface LoadingProps {
    children: ReactNode;
    className?: string;
}

const Loading : React.FC<LoadingProps> = ({children, className}) => {
    return (
        <div className={`blink ${className}`}>
            {children}
        </div>
    );
};

export default Loading;