import OwlCarousel from 'react-owl-carousel';
import { MovieState } from '../../features/hooks/useFetchMovieList';
import { Link } from 'react-router-dom';
import Loading from './Loading';

const responsiveCarouselItems = {
    0: {
        items: 3
    },
    768: {
        items: 4
    },
    1024: {
        items: 5,
        nav: true,
    },
}


const Carousel = ({list} : React.PropsWithChildren<{ list: MovieState[]}>) => {
    if(list?.length === 0){
        return(
            <Loading>Loading ...</Loading>
        )
    }
    return(
    <OwlCarousel 
        className='owl-theme mt-2' 
        loop margin={10} 
        nav={false} 
        lazyLoad={true} 
        autoplay={true}
        responsive={responsiveCarouselItems}
    >
        {list?.map((item) => (
            <Link to={`/movie_details/${item.id}`} key={item.id}>
                <div className='flex flex-col items-center justify-center'>
                    <img src={`https://image.tmdb.org/t/p/original${item.poster_path}`} alt={item.poster_path} 
                    className='w-full h-auto object-cover rounded-md
                    lg:w-auto object-top'
                    />
                    <div className={`truncate w-full text-center`}>
                        {item.title}
                    </div>
                </div>
            </Link>
        ))}
    </OwlCarousel>
    )
}

export default Carousel;