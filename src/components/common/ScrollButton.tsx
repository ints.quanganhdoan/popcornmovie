import {FaCaretSquareUp} from 'react-icons/fa'; 
import Button from './Button';

export const scrollToTop = () => { 
	window.scrollTo({ 
	top: 0, 
	behavior: 'smooth'
	}); 
}; 

const ScrollButton = () => {
return ( 
	<Button className='bg-white fixed bottom-[10%] right-0 z-10 p-1 opacity-60 hover:opacity-100'> 
	    <FaCaretSquareUp 
        onClick={scrollToTop} 
        className='text-primaryColor text-2xl lg:text-3xl'
        /> 
	</Button> 
); 
} 

export default ScrollButton; 
