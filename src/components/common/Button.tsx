import React, { ReactNode } from 'react';

interface ButtonProps {
    children: ReactNode,
    className?: string,
    onClick?: () => void,
    disabled?: boolean,
    type?: "submit" | "reset" 
}

const Button : React.FC<ButtonProps> = ({children, className, ...props}) => {
    return (
        <button className={`flex justify-center items-center gap-x-3 ${className}`} {...props}>
            {children}
        </button>
    );
};

export default Button;
