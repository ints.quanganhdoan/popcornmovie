import { Link } from "react-router-dom";
import { MovieVerticalState } from "../features/MovieList/recommendSlice";
import Loading from "./common/Loading";
import useImageErrorHandling from "../hooks/useImageErrorHandling";
import { MovieSearchState } from "../features/MovieSearch/searchPageSlice";
import { useState } from "react";


const ListMovie = ({list, textColor} : React.PropsWithChildren<{ list: MovieVerticalState[]|MovieSearchState[], textColor?:string}>) => {

    const handleImageError = useImageErrorHandling('/Error/image_not_found.png');
    const[noResult, setNoResult] = useState<boolean>(false);
    
    if (list?.length === 0) {
        setTimeout(() => {
            setNoResult(true);
        }, 2000);

        return (
            <>
                {noResult == false ? 
                (<Loading className={`text-center mt-2 ${textColor}`}>Loading...</Loading>)
                :
                (<div className={`text-center mt-2 font-bold italic h-screen ${textColor}`}>No result was found</div>)
            }
            </>
        );
    }

    return (
        <div>
            {
                list?.map((item) => (
                    <Link to={`/movie_details/${item.id}`} key={item.id}>
                        <div className="flex items-stretch bg-white mt-[6px] border border-primaryColor">
                            <img 
                                src={`https://image.tmdb.org/t/p/original${item.poster_path}`} 
                                alt={item.title} 
                                className="w-32 h-48 p-2
                                lg:w-52 lg:h-[304px]"
                                onError={handleImageError}
                            />
                            <div className="flex flex-1 flex-col-space-between p-2 text-primaryColor text-sm min-w-0">
                                <div>
                                    <div className="truncate">{item.title||item.name}</div>
                                    <p className="text-gray-400 text-sm">{item.release_date}</p>
                                </div>
                                <div className="multi-line-truncate">{item.overview}</div>
                                <p>IMDb: {item.vote_average ? item.vote_average : 0}</p>
                            </div>
                        </div>
                    </Link>
                ))
            }
        </div>
    );
};

export default ListMovie;